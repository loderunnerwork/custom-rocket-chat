# Rocket.Chat Mobile Windows Installation Guide

## Environments
1. Node.js 18.12.1 LTS.
2. Yarn 1.22.19.
3. JDK 11.
4. Android SDK 30.0.2.
5. Gradle 7.0.4.
6. NDK 21.4.7075529

## Installation
1. Install Android studio to install SDK and NDK through SDK Manager.
2. Install Node.js form official website.
3. Install Yarn for Node.js >= 16.10 by typing ```corepack enable```
4. Run the following commands to allow scripts execution(Windows only)  
```Set-ExecutionPolicy -ExecutionPolicy Unrestricted```  
```Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted```
5. In ```custom-rocket-chat/android/gradle.properties``` change ```org.gradle.java.home``` variable or remove it completely and add java path to envvars.
6. In ```custom-rocket-chat/android/local.properties``` change ```sdk.dir``` variable or remove it completely and add sdk path to envvars.
7. Add sdk's platform-tools to envvars so build can run adb.

## Build & Run
1. Connect a device or install emulator.
2. ```cd custom-rocket-chat```
3. ```yarn```
4. ```yarn android```